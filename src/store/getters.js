/* eslint-disable */

export default {
    animalsCount: (state) => {
        return state.cats.length + state.dogs.length
    },
    getAllCats: (state) => {
        return state.pets.filter((pet) => {
            console.log(pet)
            return pet.species === 'cat'
        })
    }
}