/* eslint-disable */

import Vue from 'vue'
import Vuex from 'vuex'

import actions from './actions'
import mutations from './mutations'
import state from './state'
import getters from './getters'


Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cats: state.cats,
    dogs: state.dogs,
    pets: [...state.dogs, ...state.cats]
  },
  mutations,
  actions,
  getters,
  modules: {
  }
})
