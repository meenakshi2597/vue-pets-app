import { shallowMount } from '@vue/test-utils'
import home from '@/views/Home.vue' // component to check

describe('AppHeader', () => {
  test('if button is clicked form will be visible', () => {
    const wrapper = shallowMount(home) // shallowmount is to test only particular component
    // wrapper.setData({ showPetForm: true });
    wrapper.find('button.btn').trigger('click')
    expect(wrapper.vm.showPetForm).toBe(true) // showPetForm is a boolean to enable form
  })
  // test('form is visible', () => {
  //   expect(wrapper.find('b-form').isVisible()).toBe(true)
  // });
})
